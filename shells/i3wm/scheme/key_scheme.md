# Chet's i3 Key-Binding Scheme

I'm currently (2015-07-31) on a Lenovo ThinkPad T440p. Since I take
this laptop almost everywhere I go, it makes a lot of sense for me 
to accomodate the laptop keypad as well when creating the keybindings.

At home I curtrently use a A4Tech standard keyboard (rubber dome). 
It's actually fine except for the times that the keys lock up when I
type at high speeds... I could use on of those UniComp keyboards.

## The ThinkPad Keyboard

The 440p has a pretty small form factor compared to your "standard" 
laptop; there's no:

  * numeric keypad
  * absence of a Meta on the right hand side
 
It's also annoying that the function keys are made in such a way that
they're actually media keys that *double* as function keys if you 
switch the FnLk on.

## Rules to follow:

  1. ModKey is Meta.
  2. Use `bindcode` instead of `bindsym` where appropriate.
  3. Use `ctrl` for things like closing, moving, and changing layouts.
  4. `rctl` for i3wm control, and `lctl` for client/container control.
  5. Try not to use alphabetic keys, or the numpad.
  6. Try not to add bindings to applications, the only exception to
     this is the terminal.
  7. Workspace switching doesn't need `ctrl`.
  8. Client/container focussing doesn't need `ctrl`.

## Keys

  * `Mod + PgUp`: focus parent container
  * `Mod + PgDown`: enter child container
  * `Mod + ARROW_KEYS`: focus client/container
  * `Mod + 0...9`: standard workspaces
  * `Mod + Shift + 0...9`: extra workspaces (not meant to move stuff 
     to)
  * `Mod + RCtl + Esc`: exit i3
  * `Mod + RCtl + Tilde`: reload i3
  * `Mod + RCtl + Shift + Tilde`: restart i3
  * `Mod + LCtl + Spacebar`: float client/container
  * `Mod + LCtl + Shift + Spacebar`: fullscreen the client/container
  * `Mod + LCtl + 0...9`: move client/container to standard workspace
  * `Mod + LCtl + ARROW_KEYS`: move client/container
  * `Mod + LCtl + Esc`: kill client/container
  * `Mod + LCtl + Equals`: stacking
  * `Mod + LCtl + Shift + Equals`: tabbed
  * `Mod + LCtl + Enter`: default layout
  * `Mod + LCtl + Bar`: toggle split (why bother with horizontal and 
     vertical?)
  * `Mof + Tab`: focus toggle detween floating and auto
  * `Mod + Enter`: spawn primary terminal
  * `Mod + Shift + Enter`: run dialog (not WM stuff)
