# Chet's prompt (oh well...)

# > setting variables
laptop_hostname="cain.nod"

# > checking to see if I'm on my laptop
if [ $(hostname) == "$laptop_hostname" ]
then
	pre_prompt="─── ${laptop_hostname}"
else
	pre_prompt="┄┄┄ $(hostname)"
fi

# > setting spaces
pre_prompt_space="$(sed 's/./─/g' <<< $pre_prompt)"
pre_prompt_debug="$(sed 's/./+/g' <<< $pre_prompt)"

# > temporary variables
# because for some reason PS3 has different way os escaping
tmp_white=$'\e[1;37m'
tmp_reset=$'\e[0m'

PS1="\[${col_b_yellow}\]${pre_prompt} \[${col_b_white}\]\$\[${col_reset}\] "
PS2="\[${col_b_white}\]${pre_prompt_space} ↳\[${col_reset}\] "
PS3="${tmp_white}${pre_prompt_space} ? ${tmp_reset}"
PS4="\[${col_b_green}\]${pre_prompt_debug} \$\[${col_reset}\] "
