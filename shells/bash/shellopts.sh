# Chet's shell options

# > 'set' options
set -C
set -o vi

# > 'shopts'
shopt -s cdspell
