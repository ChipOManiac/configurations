# Chet's BASh RC file (iter. 20150514-171015)

# > stuff in the standard bashrc
# interactive check
case $- in
	*i*) ;;
	  *) return ;;
esac

# > variables
# this config
bash_config_dir="${HOME}/Configs/shells/bash/"

# > files that have to be sourced
# third party
source "$bash_config_dir/shellopts.sh"
source "$bash_config_dir/colors.sh"
source "$bash_config_dir/envvars.sh"
source "$bash_config_dir/aliases.sh"
source "$bash_config_dir/userfuncs.sh"
source "$bash_config_dir/prompt.sh"
