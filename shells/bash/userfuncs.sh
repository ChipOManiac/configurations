# Chet's BASh user functions

# > mac - make and cd
# make a directory and cd into it. I'm tired of doing a consecutive cd after a
# mkdir
function mac {
	echo "MOVED TO NEW DIR $1"
	mkdir $1 && cd $1
}

# > chmox - toggle execute bit
# lazy again
function chmox {
	if [ $(stat -c '%A' $1 | cut -b 4 | tr 'X' 'x') == 'x' ]
	then
		chmod -x $1
	else
		chmod +x $1
	fi
}

# > wrsh - creates a new script with executable permissions
function wrsh {
	$EDITOR $1

	if [ -e $1 ]
	then
		chmod +x $1;
	fi
}
