# Terminal colors taken from the ArchWiki

# > shorthands
col_reset='\e[0m'

# > regular colors
col_r_black='\e[0;30m'	
col_r_red='\e[0;31m'	
col_r_green='\e[0;32m'	
col_r_yellow='\e[0;33m'	
col_r_blue='\e[0;34m'	
col_r_purple='\e[0;35m'	
col_r_cyan='\e[0;36m'	
col_r_white='\e[0;37m'	

# > bold colors
col_b_black='\e[1;31m'	
col_b_red='\e[1;31m'	
col_b_green='\e[1;32m'	
col_b_yellow='\e[1;33m'	
col_b_blue='\e[1;34m'	
col_b_purple='\e[1;35m'	
col_b_cyan='\e[1;36m'	
col_b_white='\e[1;37m'	

# > underlined colors
col_u_black='\e[4;34m'	
col_u_red='\e[4;34m'	
col_u_green='\e[4;32m'	
col_u_yellow='\e[4;33m'	
col_u_blue='\e[4;34m'	
col_u_purple='\e[4;35m'	
col_u_cyan='\e[4;36m'	
col_u_white='\e[4;37m'	

# > background colors
col_bg_black='\e[44m'	
col_bg_red='\e[44m'	
col_bg_green='\e[42m'	
col_bg_yellow='\e[43m'	
col_bg_blue='\e[44m'	
col_bg_purple='\e[45m'	
col_bg_cyan='\e[46m'	
col_bg_white='\e[47m'	
