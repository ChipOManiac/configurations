# Chet's BASh aliases

# > colorize applications
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# > shortcut commands
# list directories
alias ll='ls -l'
alias la='ls -A'
alias l1='ls -1'
alias l='ls -CF'
alias up='cd ..'
alias sd='cd -'

# process control
alias psproc='kill -s SIGSTOP'
alias reproc='kill -s SIGCONT'

# mpv player
alias wmpv="mpv --wid=$WINDOWID"
alias bmpv='mpv --wid=0'
alias mmpv='mpv --ao=null'

# feh image viewer
alias feh='feh -d'
alias Feh='feh -F'
alias seh='feh -F *'
alias reh='feh -F **'

# habitual commands with new counterparts
alias vim='vis'

# su-able commands (get it?)
alias nmtui="su -c nmtui"

# > translations for quickness (oh scheisse!)
alias klar='clear'
alias maeth='su -c "shutdown -h now"'
alias amaeth='su -c "reboot"'
