# Chet's Environment Variables

# > paths
export PATH="$PATH:/opt/bin:$HOME/Execs"

# > languages/scripts
export LANG='en_US.UTF-8'
export LOCALE='UTF-8'

# > default applications
export EDITOR='vis'
export PAGER='most'

# > programming envvars
export GOPATH="$HOME/Code/Workspaces/golang/"

# > terminal stuff (temporary)
export TERM="xterm-256color"

# > libvirt envvars
export VIRSH_DEFAULT_CONNECT_URI="qemu:///system"