# Chet's MKSh user functions

# > extgl - toggles the executable bit
#   same as chmox in the bash config
function extgl {
  [[ $(stat -c '%A' $1 | cut -b4 | tr 'X' 'x') == 'x' ]] && chmod -x $1 || chmod +x $1;
}

# > mac - make a directory and cd into it.
function mac { mkdir $1 && cd $1 && pwd; }

# > newscpt - opens editor to new script and sets the execute bit
function newscpt {
  ${EDITOR} $1
  [[ -e $1 ]] && extgl $1;
}

### DON'T GO OVER THIS LINE ###
