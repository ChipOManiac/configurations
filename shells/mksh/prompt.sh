# Chet's MKSh prompt (oh well...)

# > hostnames for physical machines I own
#   (Yes, they're based on Biblical myths...)
laptop_hstname='cain.nod';
roamer_hstname='hanoch.nod';

# > prompt-highlight
prompt_hilight="•••";

# > set the identifier mark for logged machine
case "$( hostname )" in
  $laptop_hstname)  id_mrk=' ♦' ;;
  $roamer_hstname)  id_mrk=' ⋄' ;;
  *)                id_mrk=' ×' ;;
esac

# > prompt spacing strings
prompt_pre="${prompt_hilight}${id_mrk} $(hostname)"
prompt_spc="$( sed 's/./ /g' <<< ${prompt_pre} )";
prompt_dbg="$( sed 's/./+/g' <<< ${prompt_pre} )";

# > "and now for Sir Gerald..."
#   if I'm logged in remotely mark that instead
[[ $SSH_CONNECTION ]] && prompt_pre="${prompt_hilight}${txt_bli_grn} • ${txt_cyn}$(hostname)";

PS1="${txt_ylw}${prompt_pre} ${txt_bld_wht}\$ ${txt_rst}";
PS2="${txt_wht}${prompt_spc} ${txt_bld_wht}> ${txt_rst}";
PS3="${txt_wht}Your selection? ${txt_rst}";
PS4="${txt_grn}${prompt_dbg} ${txt_bld_grn}+ ${txt_rst}";

### DON'T GO OVER THIS LINE ###
