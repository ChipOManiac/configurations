# Chet's MKSh RC file (iter. 20160321-112300)

# > variables
cfg_dir="${HOME}/Configs";
mksh_cfg_dir="${cfg_dir}/shells/mksh";

# > source
#   common files across shells
source "${cfg_dir}/shells/common/envvars.sh";
source "${cfg_dir}/shells/common/aliases.sh";

#   mksh specific files
source "${mksh_cfg_dir}/txtfx.sh";
source "${mksh_cfg_dir}/func.sh";

source "${mksh_cfg_dir}/prompt.sh";

### DON'T GO OVER THIS LINE ###
