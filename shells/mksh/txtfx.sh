# Terminal effects taken from the ArchWiki

# > reset
txt_rst="$( echo -e '\e[0m' )";

# > regular
txt_blk="$( echo -e '\e[0;30m' )";
txt_red="$( echo -e '\e[0;31m' )";
txt_grn="$( echo -e '\e[0;32m' )";
txt_ylw="$( echo -e '\e[0;33m' )";
txt_blu="$( echo -e '\e[0;34m' )";
txt_ppl="$( echo -e '\e[0;35m' )";
txt_cyn="$( echo -e '\e[0;36m' )";
txt_wht="$( echo -e '\e[0;37m' )";

# > bold
txt_bld="$( echo -e '\e[1m' )";
txt_bld_blk="$( echo -e '\e[1;30m' )";
txt_bld_red="$( echo -e '\e[1;31m' )";
txt_bld_grn="$( echo -e '\e[1;32m' )";
txt_bld_ylw="$( echo -e '\e[1;33m' )";
txt_bld_blu="$( echo -e '\e[1;34m' )";
txt_bld_ppl="$( echo -e '\e[1;35m' )";
txt_bld_cyn="$( echo -e '\e[1;36m' )";
txt_bld_wht="$( echo -e '\e[1;37m' )";

# > italics
txt_ita="$( echo -e '\e[3m' )";
txt_ita_blk="$( echo -e '\e[3;30m' )";
txt_ita_red="$( echo -e '\e[3;31m' )";
txt_ita_grn="$( echo -e '\e[3;32m' )";
txt_ita_ylw="$( echo -e '\e[3;33m' )";
txt_ita_blu="$( echo -e '\e[3;34m' )";
txt_ita_ppl="$( echo -e '\e[3;35m' )";
txt_ita_cyn="$( echo -e '\e[3;36m' )";
txt_ita_wht="$( echo -e '\e[3;37m' )";

# > underlined
txt_und="$( echo -e '\e[4m' )";
txt_und_blk="$( echo -e '\e[4;30m' )";
txt_und_red="$( echo -e '\e[4;31m' )";
txt_und_grn="$( echo -e '\e[4;32m' )";
txt_und_ylw="$( echo -e '\e[4;33m' )";
txt_und_blu="$( echo -e '\e[4;34m' )";
txt_und_ppl="$( echo -e '\e[4;35m' )";
txt_und_cyn="$( echo -e '\e[4;36m' )";
txt_und_wht="$( echo -e '\e[4;37m' )";

# > blinking
txt_bli="$( echo -e '\e[5m' )";
txt_bli_blk="$( echo -e '\e[5;30m' )";
txt_bli_red="$( echo -e '\e[5;31m' )";
txt_bli_grn="$( echo -e '\e[5;32m' )";
txt_bli_ylw="$( echo -e '\e[5;33m' )";
txt_bli_blu="$( echo -e '\e[5;34m' )";
txt_bli_ppl="$( echo -e '\e[5;35m' )";
txt_bli_cyn="$( echo -e '\e[5;36m' )";
txt_bli_wht="$( echo -e '\e[5;37m' )";

### DO NOT GO OVER THIS LINE ###
