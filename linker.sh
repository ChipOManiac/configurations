#!/usr/bin/env bash

# linker.sh ( all | app1 | ... ) [ map_file ]
# makes hard links of configs in the TLD to places the user specifies
# in `map.txt`.
#
# `map.txt` example
# ---
# teksh shell/tek/tekrc ~/.tekrc
# foo applications/foo/foo.conf ~/.config/foo.conf
# ---

# > variables
def_map_file="${PWD}/map.txt"

# > MAIN SEQUENCE
# check to see if any arguments are supplied
if [ $# == 0 ]
then
	echo "What am I supposed to do then?"
	exit 1
fi

# okay? then get the args
apps="$1"

# if there's a map get it
if [ $# == 2 ]
then
	if [ ! -f $2 ]
	then
		echo "Um, that file does NOT exist"
		exit 1
	fi
	map_file="$2"
else
	map_file="$def_map_file"
fi

# determine content of $apps
if [ $apps == 'all' ]
then
	ex="*"
else
	ex="$apps"
fi

# iterate and link
grep $ex $map_file | while read line
do
	src="$(cut -d\  -f2 <<< $line)"
	out="$(cut -d\  -f3 <<< $line)"

	ln $src $out
done
